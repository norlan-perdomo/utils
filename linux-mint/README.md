# Linux Mint install script

Use the following script which will download and run the `install.sh` script
``` sh
rm -f /tmp/utils.tar.gz && \
rm -rf /tmp/utils-master-* && \
wget -O /tmp/utils.tar.gz https://gitlab.com/norlan-perdomo/utils/repository/master/archive.tar.gz && \
tar xvzf /tmp/utils.tar.gz -C /tmp && \
mv /tmp/utils-master-* /tmp/utils && \
/tmp/utils/linux-mint/install.sh
```