#!/bin/sh

log ()
{
    echo "----------------------------"
    echo "--$1--"
    echo "----------------------------"
}

log 'changing to home directory'
cd ~

log 'enable i386 architecture'
sudo dpkg --add-architecture i386

log 'adding ppa for openrazer'
sudo add-apt-repository ppa:openrazer/stable

log 'adding ppa for polychromatic'
sudo add-apt-repository ppa:lah7/polychromatic

log 'adding ppa for unetbootin'
sudo add-apt-repository ppa:gezakovacs/ppa

log 'adding ppa for veracrypt'
sudo add-apt-repository ppa:unit193/encryption

log 'add pomodoro release key'
curl -L https://download.opensuse.org/repositories/home:/kamilprusko/xUbuntu_16.04/Release.key | sudo apt-key add -

log 'add pomodoro sources list file'
sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/kamilprusko/xUbuntu_16.04/ /' > /etc/apt/sources.list.d/gnome-pomodoro.list"

log 'add nodejs resources in order to be able to install it through apt install'
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

log 'remove any instance of docker'
sudo apt remove docker docker-engine docker.io

log 'add docker GPG key'
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

log 'plex sources list file'
echo deb https://downloads.plex.tv/repo/deb ./public main | sudo tee /etc/apt/sources.list.d/plexmediaserver.list

log 'plex signing key'
curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key add -

log 'add ppa for docker'
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   xenial \
   stable"

log 'add ppa for peek'
sudo add-apt-repository ppa:peek-developers/stable

log 'add ppa for rvm'
sudo apt-add-repository -y ppa:rael-gc/rvm

log 'calling apt install with all packages'
sudo apt update && \
    sudo apt upgrade -y && \
    sudo apt install \
    software-properties-common \
    handbrake \
    pianobar \
    xsane \
    python3 \
    python3-pip \
    python-pip \
    git \
    brasero \
    sublime-text \
    dropbox \
    pinta \
    build-essential \
    libc6:i386 \
    libncurses5:i386 \
    libstdc++6:i386 \
    lib32z1 \
    libbz2-1.0:i386 \
    python3-razer \
    razer-kernel-modules-dkms \
    razer-daemon \
    razer-doc \
    polychromatic \
    redshift \
    redshift-gtk \
    geoclue-2.0 \
    spotify-client \
    openssh-server \
    remmina \
    remmina-plugin-rdp \
    shutter \
    gnome-pomodoro \
    nodejs \
    unetbootin \
    skypeforlinux \
    libgoo-canvas-perl \
    ruby-dev \
    virtualbox \
    apt-transport-https \
    ca-certificates \
    veracrypt \
    docker-ce \
    plexmediaserver \
    peek \
    audacity \
    rvm \
    -y \

log 'downloading latest chrome deb package'
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

log 'calling dpkg on chrome deb package'
sudo dpkg -i google-chrome*.deb

log 'using pip to install b2(back blaze cli tools)'
sudo pip install setuptools b2

log 'upgrade pip3'
sudo pip3 install --upgrade pip

log 'using pip3 to install virtualenv'
sudo pip3 install virtualenv

log 'removing any pianobar config'
rm -r /home/$USER/.config/pianobar

log 'make pianobar config directory try in home directory'
mkdir -p /home/$USER/.config/pianobar

log 'create config for pianobar'
touch /home/$USER/.config/pianobar/config

log 'writing pianobar values in pianobar config'
log 'enter pandora email:'
read pandora_email
log 'enter pandora password:'
read pandora_password
echo "user = $pandora_email" >> /home/$USER/.config/pianobar/config
echo "password = $pandora_password" >> /home/$USER/.config/pianobar/config

#razer deathabe mouse
#this is for the razer deathaber mouse daemon to work correctly
#ADD YOUR USER TO THE PLUGDEV GROUP '
sudo gpasswd -a $USER plugdev

#plex
#change permission to plex server so that it can start up
sudo chown -R $USER:$USER /usr/lib/plexmediaserver
sudo chown -R $USER:$USER /var/lib/plexmediaserver

log 'remove redshift config if exists'
rm -f /home/$USER/.config/redshift.conf

#redshift configuration file
log 'create redshift conf file'
touch /home/$USER/.config/redshift.conf

log 'append values to redshift conf file'
echo "; Global settings for redshift" >> /home/$USER/.config/redshift.conf
echo "[redshift]" >> /home/$USER/.config/redshift.conf
echo "; Set the day and night screen temperatures" >> /home/$USER/.config/redshift.conf
echo "temp-day=3500" >> /home/$USER/.config/redshift.conf
echo "temp-night=3500" >> /home/$USER/.config/redshift.conf
echo "location-provider=manual" >> /home/$USER/.config/redshift.conf
echo "[manual]" >> /home/$USER/.config/redshift.conf
echo "lat=48.1" >> /home/$USER/.config/redshift.conf
echo "lon=11.6" >> /home/$USER/.config/redshift.conf

log 'check if /usr/share/applications directory exists'
if [ ! -d "/usr/share/applications" ]; then
  log '/usr/share/applications doesn''t exist, creating it'
  sudo mkdir -p /usr/share/applications
fi

log 'removing postman install'
sudo rm -rf /opt/Postman
sudo rm /usr/share/applications/Postman.desktop

log 'removing postman tar if exists'
rm -f postman.tar.gz

log 'downloading lastest postman'
wget --output-document=postman.tar.gz https://dl.pstmn.io/download/latest/linux64

log 'untaring latest postman'
tar -zxvf postman.tar.gz

log 'moving extracted postman directory to opt'
sudo mv Postman/ /opt/

log 'removing postman tar'
rm postman.tar.gz

log 'create desktop entry for postman'
echo "[Desktop Entry]
Name=Postman
Exec=/opt/Postman/Postman
StartupNotify=true
Terminal=false
Type=Application" > Postman.desktop

log 'remove postman desktop file if exists'
rm -f /usr/share/applications/Postman.desktop

log 'moving postman desktop file to /usr/share/applications/'
sudo mv Postman.desktop /usr/share/applications/

log "enter android-studio download url"
read url

log "removing android-studio.zip if exists"
rm -rf android-studio.zip

log "downloading android studio from $url"
wget -O android-studio.zip $url

log "removing android-studio directory"
rm -rf android-studio
sudo rm -rf /opt/android-studio

log "extracting android-studio.zip"
unzip android-studio.zip

log "moving android-studio directory to /opt"
sudo mv android-studio /opt

log "starting android studio"
/opt/android-studio/bin/studio.sh

log "enter intellij download url"
read url

log "untar intellij"
tar -zxvf ideaIU-*

log "rename untared intellij folder"
mv idea-* idea

log "move intellij to /opt"
sudo mv idea /opt

log "starting intellij"
/opt/idea/bin/idea.sh

log "install brother driver deb package"
sudo dpkg -i /tmp/utils/linux-mint/brother-scanner/brscan4-0.4.2-1.amd64.deb && sudo apt install -f

log 'removing sym link for brother scanner script if exists'
sudo rm /usr/local/bin/brother-scanner.sh

log "create 'brother-scanner' directory inside the '/opt' directory"
sudo mkdir /opt/brother-scanner

log "move 'permissions.py' into '/opt/brother-scanner' directory"
sudo mv /tmp/utils/linux-mint/brother-scanner/permissions.py /opt/brother-scanner/permissions.py

log "move 'scanner.sh' into '/opt/brother-scanner' directory"
sudo mv /tmp/utils/linux-mint/brother-scanner/scanner.sh /opt/brother-scanner/scanner.sh

log "creating sym link for brother scanner script"
sudo chmod a+x /opt/brother-scanner/scanner.sh
sudo ln -s /opt/brother-scanner/scanner.sh /usr/local/bin/brother-scanner.sh
