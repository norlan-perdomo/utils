import subprocess

result = subprocess.check_output(['lsusb'])
lines = result.decode('utf-8').split('\n')
for line in lines:
    if 'brother industries' in line.lower():
        print(line)

        busIndex = line.index('Bus')
        bus = line[busIndex+4:busIndex+7]

        deviceIndex = line.index('Device')
        device = line[deviceIndex+7:deviceIndex+10]

        subprocess.check_output(['chmod', '666', '/dev/bus/usb/{0}/{1}'.format(bus, device)])
