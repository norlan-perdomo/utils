#!/usr/bin/env zsh
gcloud compute instances delete "my-fastai-instance" \
        --zone="us-east1-c"