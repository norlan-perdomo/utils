const { JWT } = require('google-auth-library');

module.exports = async () => {
  const config = require('../config.json');
  const SCOPES = ['https://www.googleapis.com/auth/compute'];

  const jwtClient = new JWT({
    email: config.client_email,
    key: config.private_key,
    scopes: SCOPES
  });

  const response = await jwtClient.authorize();
  return response.access_token;
};