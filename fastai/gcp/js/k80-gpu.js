const axios = require('axios');
const auth = require('./helpers/auth_token');
const config = require('./config.json');

auth()
  .then(token => {
    const url = `https://compute.googleapis.com/compute/v1/projects/${config.project_id}/zones/${config.zone}/instances/${config.instance_id}/setMachineResources`;
    const options = { headers: { "Authorization": `Bearer ${token}` } };
    const data = {
      "guestAccelerators": [
        {
          "acceleratorCount": 1,
          "acceleratorType": `https://compute.googleapis.com/compute/v1/projects/${config.project_id}/zones/${config.zone}/acceleratorTypes/nvidia-tesla-k80`
        }
      ]
    };
    axios.post(url, data, options).finally(() => console.log("finished"));
  })
  .catch(err => {
    console.log(err)
  });