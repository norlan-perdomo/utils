#!/usr/bin/env zsh
gcloud compute instances create "my-fastai-instance" \
        --zone="us-east1-c" \
        --image-family="pytorch-latest-gpu" \
        --image-project=deeplearning-platform-release \
        --maintenance-policy=TERMINATE \
        --accelerator="type=nvidia-tesla-t4,count=1" \
        --machine-type=""n1-highmem-4"" \
        --boot-disk-size=50GB \
        --metadata="install-nvidia-driver=True"
gcloud compute config-ssh