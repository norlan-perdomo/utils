#!/usr/bin/env zsh
echo "---------------------------------------------------"
echo ""
echo "when connected, navigate to http://localhost:8080/tree"
echo ""
echo "---------------------------------------------------"
gcloud compute config-ssh
gcloud compute ssh --zone=us-east1-c jupyter@my-fastai-instance -- -L 8080:localhost:8080