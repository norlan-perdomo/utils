#!/usr/bin/env zsh
az group deployment create \
  --name $AZURE_VM_NAME \
  --resource-group $AZURE_GROUP \
  --template-file fastai_template.json \
  --parameters adminUsername=$AZURE_VM_USERNAME adminPassword=$AZURE_VM_PASSWORD vmName=$AZURE_VM_NAME vmSize=$AZURE_VM_SIZE
./status.sh