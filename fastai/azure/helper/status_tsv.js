process.argv.map((arg, index) => {
  if (arg === '--data') {
    const data = process.argv[index + 1].split('\t');
    process.stdout.write(`jupyter notebook: 'https://${data[1]}:8000'\n`);
    process.stdout.write(`status: ${data[2]}\n`);
    process.stdout.write(`VM size: ${data[3]}\n`);
  }
});