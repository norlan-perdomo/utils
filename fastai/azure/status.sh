#!/usr/bin/env zsh
json=$(az vm list -d -o tsv --query "[?name=='"$AZURE_VM_NAME"'].[name,publicIps,powerState,hardwareProfile.vmSize]")

echo '-----------------------------------------\n'
node helper/status_tsv.js --data $json
echo \
'vm username: ' $AZURE_VM_USERNAME '\n'\
'vm password: ' $AZURE_VM_PASSWORD\
'\n\n'\
'-----------------------------------------'