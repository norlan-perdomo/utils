# Requirements

Have the following exports in the .zshrc file
```shell script
export AZURE_VM_USERNAME='username'
export AZURE_VM_PASSWORD='password'
export AZURE_VM_NAME='vm_name'
export AZURE_VM_SIZE='Standard_NC6 (1 Nvidia K80 GPU, 6 vCPU)'
export AZURE_GROUP='group'
```