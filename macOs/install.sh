#install nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash


/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew install \
pyenv \
pipenv \
wget \
gpg \
mas \
azure-cli \
gnupg \
tree \
tldr \
yarn \
rclone \
java11

brew install -- cask spectacle \
android-studio \
intellij-idea \
1password \
sublime-text \
tomighty \
cryptomator \
whatsapp \
transmission \
spotify \
vlc \
docker \
gimp \
tor-browser \
skype \
firefox \
homebrew/cask-drivers/wacom-tablet \
google-cloud-sdk \
balenaetcher \
zoom \
mullvadvpn \
telegram \
signal \
postman \
discord \
notion \
bitwarden \
gpg-suite \
syncthing \
element \ 
thunderbird \
http-toolkit \
handbrake \
vysor \
appcleaner

# mas install app ids
team_paper_snap=1199502670

#mas installs (app store automated installs)
mas install $team_paper_snap
